'use strict'

const services = require('../services')

function isAuth(req,res, next){
  console.log(req.headers.authorization)
  if(!req.headers.authorization){
    return res.status(403).send({message:'No tienes autorizacion'})
  }
  const token = req.headers.authorization.replace(/['"]+/g,'')
  console.log(token)
services.decodeToken(token).
  then(response =>{
    console.log('PROMESA DECODE')
  req.user = response
  next()
  })
  .catch(response => {
  res.status(response.status)
  })
}

module.exports = isAuth
