'use strict'

const express = require('express')
const productCtrl = require('../controllers/product')
const userCtrl = require('../controllers/user')
const inventaCtrl = require('../controllers/inventario')
const auth = require('../middelware/auth')
const api = express.Router()
/*
getInventarios,
saveInventario,
updateInventario,
deleteInventario
*/
//Muestra la lista de productos en el inventario
api.get('/listinventarios',auth,inventaCtrl.getInventarios)
//realiza un consulta de un solo producto (para uso unico del administrador)
api.get('/oneproduct/:productId',auth,inventaCtrl.getOneProduct)
//Agrega un nuevo producto al inventario (para uso unico del administrador)
api.post('/inventory',inventaCtrl.saveInventario)
//Actualiza un producto del inventario (para uso unico del administrador)
api.put('/inventory/:inventoryId',inventaCtrl.updateInventario)
//Borra un producto del inventario (para uso unico del administrador)
api.delete('inventory/:inventoryid',inventaCtrl.deleteInventario)
//Obtiene la lista de productos favoritos de los clientes
api.get('/product',productCtrl.getProducts)
//Obtiene la lista de productos favorita de un cliente
api.get('/product/:productId',auth,productCtrl.getProduct)
//Guarda un producto en la lista de favoritos del cliente
api.post('/product',auth,productCtrl.saveProduct)
//Da de alta un cliente en la Base
api.post('/signUp',userCtrl.signUp)
//Inicio de Sesion
api.post('/signIn',userCtrl.signIn)
//Actualiza el comentario de un producto
api.put('/product/:productId',productCtrl.updateProduct)
//Borra un producto de la lista del cliente
api.delete('/product/:productId',productCtrl.deleteProduct)
//Flujo de prueba
api.get('/private',auth,function(req,res){
  res.status(200).send({message:'Tienes acceso'})
})

module.exports = api
