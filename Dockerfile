#imagen base
FROM node:latest

#Directorio de la app
WORKDIR /entregaFinal

#Copia de archivos
ADD . /entregaFinal

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comando
CMD ["npm","start"]
