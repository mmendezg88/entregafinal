'use strict'
const Inventario = require('../models/inventario')

function getInventarios(req,res){
    Inventario.find({},(err,inventarios) =>{
    if(err) return res.status(500).send({message:`Error al realizar la peticion:${err}`})
    if(!inventarios) return res.status(404).send({message:'No existen Inventarioos'})
    res.status(200).send({inventarios})
  })
}

function getOneProduct(req,res){
  let productId = req.params.productId
  console.log('Consulta un solo Producto',req.params.productId)
    Inventario.findById({_id:productId}, (err,inventario) => {
      if (err) return res.status(500).send({message:`Error al realizar la peticion: ${err}`})
      if (!inventario) return res.status(404).send({message:`El producto no Existe`})
      res.status(200).send({inventario})
    })
}


function saveInventario(req,res){
  console.log('POST /api/inventario')
  console.log(req.body)
  let inventario = new Inventario()
  inventario.name = req.body.name
  inventario.picture = req.body.picture
  inventario.price = req.body.price
  inventario.category = req.body.category
  inventario.description = req.body.description
  inventario.save((err,inventarioStored) =>{
    if(err) res.status(500).send({message:`Error al salvar en la base de datos: ${err}`})

    res.status(200).send({inventario: inventarioStored})
  })
}

function updateInventario(res,req){
  let inventarioId = req.params.inventarioId
  let update = req.body

  Inventario.findByIdAndUpdate(inventarioId, update, (err,inventarioUpdated) =>{
    if (err) res.status(500).send({message:`Error al actualizar el inventarioo:${err}`})
    res.status(200).send({inventario:inventarioUpdated})
  })
}

function deleteInventario(res,req){
  let inventarioId = req.params.inventarioId

  Inventario.findById(inventarioId, (err,inventario) =>{
    if (err) res.status(500).send({message:`Error al borrar el inventarioo: ${err}`})

    inventario.remove(err =>{
      if (err) res.status(500).send({message:`Error al borrar inventarioo: ${err}`})
      res.status(200).send({message:`El inventarioo ha sido eliminado`})
    })

  })
}

module.exports = {
  getInventarios,
  getOneProduct,
  saveInventario,
  updateInventario,
  deleteInventario
}
