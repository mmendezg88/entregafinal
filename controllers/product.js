'use strict'
const Product = require('../models/product')
var mongoose = require('mongoose');

function getProduct(req,res){
  let productId = req.params.productId
  console.log('Consulta todo',req.params.productId)
    Product.find({idusuario:productId}, (err,product) => {
      if (err) return res.status(500).send({message:`Error al realizar la peticion: ${err}`})
      if (!product) return res.status(404).send({message:`El producto no Existe`})
      res.status(200).send({product})
    })
}

function getProducts(req,res){
    Product.find({},(err,products) =>{
    if(err) return res.status(500).send({message:`Error al realizar la peticion:${err}`})
    if(!products) return res.status(404).send({message:'No existen Productos'})
    res.status(200).send({products})
  })
}

function saveProduct(req,res){
  console.log('POST /api/product')
  console.log(req.body)
  let product = new Product()
  product.idusuario = req.body.idusuario
  product.name = req.body.name
  product.picture = req.body.picture
  product.price = req.body.price
  product.category = req.body.category
  product.description = req.body.description
  product.save((err,productStored) =>{
    if(err) res.status(500).send({message:`Error al salvar en la base de datos: ${err}`})

    res.status(200).send({product: productStored})
  })
}

function updateProduct(req,res){
  let productId =req.params.productId
  let update = req.body
  console.log(';;;;;;;;;;;;',update);
  //console.log('::::::::::::',req.params.productId)
  console.log('::::::::::::',productId);
  Product.findByIdAndUpdate({_id: productId},update, { new: true },(err,productUpdated) =>{
    if (err){
       res.status(500).send({message:`Error al salvar en la base de datos: ${err}`});
       throw err;
     }
     res.status(200).send({productUpdated});
     console.log("success");
  })
}

function deleteProduct(res,req){
  console.log('DELETE')
  console.log('BODY:::::',res.body)
  console.log('PARAMAS::::',res.params.productId)
  let productId = res.params.productId

  Product.deleteOne({_id:productId}, (err,product) =>{
    if (err){
       console.log("failed");
       throw err;
     }
     console.log("success");
  })
}

module.exports = {
  getProduct,
  getProducts,
  saveProduct,
  updateProduct,
  deleteProduct
}
