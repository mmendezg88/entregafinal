'use strict'

const bcrypt = require('bcrypt-nodejs')
const mongoose = require('mongoose')
const User = require('../models/user')
const service = require('../services')

function signUp(req,res){
  const user = new User({
    email:req.body.email,
    displayName:req.body.displayName,
    password: req.body.password
  })
  console.log('body',user);
  console.log('Imprime correo: ',req.body.email);
  console.log('Imprime Pass: ',req.body.password);
  console.log('Imprime Pass: ',req.body.displayName);

  user.save((err)=>{
    if(err) res.status(204).send({message:`Error al crear el usuario: ${err}`})
    return res.status(200).send({token: service.createToken(user)})
  })
}

function signIn(req,res){
  const password = req.body.ṕassword
  User.findOne({email:req.body.email},(err,user) => {
    if (err) return res.status(500).send({message:err})
    if (!user) return res.status(404).send({message:'No Existe el Usuario'})
    console.log(user.password)
                    //Comprobar contraseña
    bcrypt.compare(req.body.password, user.password, function(err, check){
    console.log(check)
    if(check){
        //Devolver datos de usuario
      if(req.body.gethash){
        //Devolver un token de jwt
        res.status(200).send({
          message:'Te has logueado Correctamente',
          token: service.createToken(user)
        })
        }else{
          res.status(200).send({
            message:'Te has logueado Correctamente',
            token: service.createToken(user),
            userid:user.id
          })
          }
      }else{
         res.status(404).send({message: "El usuario no ha podido logearse"})
        }
      })

  })
}

module.exports = {
  signUp,
  signIn
}
