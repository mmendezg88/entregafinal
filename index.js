'use strict'

const mongoose = require('mongoose')
const app = require('./app')
const config = require('./config')
//const port = process.env.PORT || 3001

/*const productCtrl = require('./controllers/product')

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())*/

mongoose.connect(config.db,(err,res)=>{
  if(err){
      return console.log(`Eror al conectar con la base de datos : ${error}`)
    }

    console.log('Conexion a la base de datos establecida....')

    app.listen(config.port, () => {
      console.log(`API REST Corriendo en http://localhost:${config.port}`)
    })
})

//PANTALLA PRINCIPAL
//app.get('/api/product',productCtrl.getProducts)
/*app.get('/api/product',(req,res) =>{
  Product.find({},(err,products) =>{
    if(err) return res.status(500).send({message:`Error al realizar la peticion:${err}`})
    if(!products) return res.status(404).send({message:'No existen Productos'})
    res.send(200,{products})
  })
})*/

//Agregar Registros
//app.get('/api/product/:productId',productCtrl.getProduct)
/* app.get('/api/product/:productId',(req,res) => {
 let productId = req.params.productId
  Product.findById(productId, (err,product) => {
    if (err) return res.status(500).send({message:`Error al realizar la peticion: ${err}`})
    if (!product) return res.status(404).send({message:`El producto no Existe`})
    res.status(200).send({product})
  })
})*/

//Consulta Tasks
//app.post('/api/product',productCtrl.saveProduct)
/*app.post('/api/product',function (req, res) {
  console.log('POST /api/product')
  console.log(req.body)
  let product = new Product()
  product.name = req.body.name
  product.picture = req.body.picture
  product.price = req.body.price
  product.category = req.body.category
  product.description = req.body.description
  product.save((err,productStored) =>{
    if(err) res.status(500).send({message:`Error al salvar en la base de datos: ${err}`})

    res.status(200).send({product: productStored})
  })
})*/


//PUT
//app.put('/api/product/:productId',productCtrl.updateProduct)
/*app.put('/api/product/:productId', function(req,res){
  let productId = req.params.productId
  let update = req.body

  Product.findByIdAndUpdate(productId, update, (err,productUpdated) =>{
    if (err) res.status(500).send({message:`Error al actualizar el producto:${err}`})
    res.status(200).send({product:productUpdated})
  })
})*/
//BORRAR DE ARCHIVO
//app.delete('/api/product/:productId', productCtrl.deleteProduct)
/*app.delete('/api/product/:productId', function (req, res) {
  let productId = req.params.productId

  Product.findById(productId, (err,product) =>{
    if (err) res.status(500).send({message:`Error al borrar el producto: ${err}`})

    product.remove(err =>{
      if (err) res.status(500).send({message:`Error al borrar producto: ${err}`})
      res.status(200).send({message:`El producto ha sido eliminado`})
    })

  })
})*/
