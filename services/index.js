'use strict'

const jwt = require('jwt-simple')
const moment = require('moment')
const config = require('../config')

function createToken(user){

  const payload = {
    sub:user._id,
    ait:moment().unix(),
    exp:moment().add(14,'days').unix()
  }
  console.log(jwt.encode(payload,config.SECRET_TOKEN))
  return jwt.encode(payload,config.SECRET_TOKEN)
}

function decodeToken(token){
  console.log('Entra a la Funcion decode')
  const decoded = new Promise((resolve,reject) =>{
    try{
        console.log('Payload PRE')
        const payload = jwt.decode(token,config.SECRET_TOKEN)
        if (payload.exp <= moment().unix()){
          reject({
            status: 401,
            message:'El token a Expirado'
          })
        }
      resolve(payload.sub)
    }catch(err){
      reject({
        status: 500,
        message: 'Invalid Token'
      })
    }
  })

  return decoded
}

module.exports = {createToken,decodeToken}
